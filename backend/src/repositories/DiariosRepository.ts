import { EntityRepository, Repository } from 'typeorm';

import Diario from '../models/Diario';

@EntityRepository(Diario)
class DiariosRepository extends Repository<Diario> {

}

export default DiariosRepository;
