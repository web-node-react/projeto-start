import { EntityRepository, Repository } from "typeorm";

import Conta from '../models/Conta';

@EntityRepository(Conta)
class ContasRepository extends Repository<Conta>{    
  public async findByIdPai(idPai: string): Promise< Conta | null>{
    const contaPai = await this.findOne({
      where:{id:idPai},
    });
    return contaPai || null;
  }  

  public async calculateIndex(pai: Conta): Promise<string>{
    const [filhos, qtdFilhos] = await this.findAndCount({idPai:pai.id});
    const indice = qtdFilhos + 1;
    return pai.indice+"."+indice || "";
  }  

}

export default ContasRepository;