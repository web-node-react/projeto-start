import {MigrationInterface, Table, QueryRunner} from "typeorm";

export default class createDiario1641928321185 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(
            new Table({
                name:'diarios',
                columns:[
                    {
                        name:'id',
                        type: 'varchar',
                        isPrimary: true,
                        generationStrategy: 'uuid', 
                        default:'uuid_generate_v1()',                   
                    },
                    {
                        name: 'data',
                        type: 'timestamp',
                        isNullable: false,
                    },
                    {
                        name: 'valor',
                        type: 'float',                        
                    },
                    {
                        name: 'descricao',
                        type: 'varchar',                        
                    },
                    {
                        name: 'contaDestino',
                        type: 'varchar',                        
                    },
                    {
                        name: 'contaOrigem',
                        type: 'varchar',
                        isNullable: true,                        
                    },
                    {
                        name: 'created_at',
                        type: 'timestamp',
                        default: 'now()',
                    },
                    {
                        name: 'updated_at',
                        type: 'timestamp',
                        default: 'now()',                        
                    },
                ],
            }), 
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable('diarios')
    }

}
