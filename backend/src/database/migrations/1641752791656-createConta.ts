import {MigrationInterface, QueryRunner, Table} from "typeorm";

export default class createConta1641752791656 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(
            new Table({
                name:'contas',
                columns:[
                    {
                        name:'id',
                        type: 'varchar',
                        isPrimary: true,
                        generationStrategy: 'uuid', 
                        default:'uuid_generate_v1()',                   
                    },
                    {
                        name: 'idPai',
                        type: 'varchar',
                    },
                    {
                        name: 'indice',
                        type: 'varchar',
                        isUnique:true,
                    },
                    {
                        name: 'nome',
                        type: 'varchar',                        
                    },
                    {
                        name: 'receita',
                        type: 'boolean',                        
                    },
                    {
                        name: 'custoFixo',
                        type: 'boolean',                        
                    },
                    {
                        name: 'created_at',
                        type: 'timestamp',
                        default: 'now()',
                    },
                    {
                        name: 'updated_at',
                        type: 'timestamp',
                        default: 'now()',                        
                    },
                ],
            }), 
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable('contas')
    }

}
