import {MigrationInterface, QueryRunner, TableColumn, TableForeignKey} from "typeorm";

export default class alterContaAutoRelacionamento1641903676231 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        //Drop coluna antiga
        await queryRunner.dropColumn("contas","idPai");

        //criar a nova coluna
        await queryRunner.addColumn("contas",
        new TableColumn({
            name:'idPai',
            type:'varchar',
            isNullable:true,
        }));

        //criar a foreignKey
        await queryRunner.createForeignKey('contas',
        new TableForeignKey({
            name:'fgContaPai',
            columnNames: ['idPai'],//novaColuna
            referencedColumnNames:['id'],
            referencedTableName:'contas',
            onDelete:'SET NULL',
            onUpdate:'CASCADE',
        }))

    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropForeignKey('contas', 'fgContaPai');



    }

}
