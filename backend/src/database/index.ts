import { createConnection } from "typeorm";

/**
 * Esta função percorre minha arvore de arquivos 
 * procurando o arquivo ormconfig.json
 *  Se encontrar
 *    vai ler o arquivo json e efetuar a conexao com BD
 */
createConnection();

/**
 * Também é possivel fazer a configuração direto neste método 
 * createConnection(){
 *    "type" : "postgres"
 *     ....
 * }
 * 
 * Porem o TypeORM tem um CLI(interface de linha de comando) e se formos utilizar outros
 * comandos do CLI, como por exemplo migrations, ele não vai
 * funcionar. Ou melhor, so vai funcionar por meio do arquivo
 * ormconfig.json 
 * 
 */