import { Router } from 'express';
import { getCustomRepository } from 'typeorm';

import ContasRepository from '../repositories/ContasRepository';
import CreateContaService from '../services/CreateContaService';

const contasRouter = Router();

contasRouter.get('/',async (request, response) => {
  const contasRepository = getCustomRepository(ContasRepository);
  const contas = await contasRepository.find();

  return response.json(contas);
});


contasRouter.post('/', async (request, response) => {
  //routes.get('/',(request, response) => response.json({message:"Ola mundo"}));
  try {
    const { idPai, nome, receita, custoFixo } = request.body;

    const createConta = new CreateContaService();
    const conta = await createConta.execute({
      idPai,
      nome,
      receita,
      custoFixo,
    });

    return response.json(conta);
  }catch(err){
    return response.status(400).json({error: "contas.routes | Erro ao criar Conta"});
  }
});

export default contasRouter;
