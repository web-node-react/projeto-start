import { Router } from 'express';
import { getCustomRepository } from 'typeorm';

import DiariosRepository from '../repositories/DiariosRepository'
import CreateDiarioService from '../services/CreateDiarioService';

const contasRouter = Router();

contasRouter.get('/',async (request, response) => {
  const diariosRepository = getCustomRepository(DiariosRepository);
  const contas = await diariosRepository.find();

  return response.json(contas);
});


contasRouter.post('/', async (request, response) => {  
  try {
    const { data, valor, descricao, contaDestino, contaOrigem } = request.body;

    const createDiario = new CreateDiarioService();
    const diario = await createDiario.execute({
      data,
      valor,
      descricao,
      contaDestino,
      contaOrigem
    });

    return response.json(diario);
  }catch(err){
    return response.status(400).json({error: "diarios.routes.ts | Erro ao criar Lancamento no diário"});
  }
});

export default contasRouter;
