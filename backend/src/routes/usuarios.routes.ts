import { Router } from 'express';
import { getCustomRepository } from 'typeorm';

import UsuariosRepository from '../repositories/UsuariosRepository';
import CreateUsuarioService from '../services/CreateUsuarioService';

const usuariosRouter = Router();

usuariosRouter.get('/',async (request, response) => {
  const usuariosRepository = getCustomRepository(UsuariosRepository);
  const usuarios = await usuariosRepository.find();

  return response.json(usuarios);
});


usuariosRouter.post('/', async (request, response) => {
  //routes.get('/',(request, response) => response.json({message:"Ola mundo"}));
  try {
    const { nome, email, password } = request.body;

    const createUsuario = new CreateUsuarioService();
    const usuario = await createUsuario.execute({
      nome,
      email,
      password,
    });

    return response.json(usuario);
  }catch(err){
    return response.status(400).json({error: "message"});
  }
});

export default usuariosRouter;
