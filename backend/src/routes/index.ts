import { Router } from 'express';
import usuariosRouter from './usuarios.routes';
import contasRouter from './contas.routes';
import diariosRouter from './diarios.routes';

const routes = Router();
routes.use('/usuarios', usuariosRouter);
routes.use('/contas', contasRouter);
routes.use('/diarios', diariosRouter);

export default routes;
