import { Entity, Column, 
  PrimaryGeneratedColumn, 
  JoinColumn,ManyToOne } from "typeorm";
import Conta from "./Conta";


@Entity('diarios')
export default class Diario {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('timestamp')
  data:Date;    

  @Column()
  valor:number;

  @Column()
  descricao:string;

  @Column()
  contaDestino: string;

  @Column()
  contaOrigem: string;  
  
  @ManyToOne(type=>Conta)
  @JoinColumn({name:'contaDestino'})
  conta:Conta;


}


//export default Conta;