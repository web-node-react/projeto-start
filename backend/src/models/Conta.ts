import { Entity, Column, 
  PrimaryGeneratedColumn, 
  JoinColumn,ManyToOne } from "typeorm";

@Entity('contas')
export default class Conta {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  idPai:string;    

  @Column()
  indice:string;

  @Column()
  nome:string;

  @Column()
  receita: boolean;

  @Column()
  custoFixo: boolean;  
  
  @ManyToOne(type=>Conta)
  @JoinColumn({name:'idPai'})
  pai:Conta;


}


//export default Conta;