import { getCustomRepository, Timestamp } from 'typeorm';
import Diario from '../models/Diario';
import DiariosRepository from '../repositories/DiariosRepository';



interface Request {
  data : Date,
  valor: number,
  descricao: string,
  contaDestino: string,
  contaOrigem: string
}

class CreateDiarioService {

  public async execute({ data, valor, descricao, contaDestino, contaOrigem }: Request): Promise<Diario> {
    const diariosRepository = getCustomRepository(DiariosRepository);

   

    //const conta = contasRepository.create({      
    //  idPai,
    //  indice,
    //  nome,
    //  receita,
    //  custoFixo,                  
    //});
    const diario = diariosRepository.create();    
    diario.data = data;
    diario.valor = valor;
    diario.descricao = descricao;    
    diario.contaDestino = contaDestino;    
    diario.contaOrigem = contaOrigem;
    await diariosRepository.save(diario);

    return diario;
  }
}

export default CreateDiarioService;