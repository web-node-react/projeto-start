import { getCustomRepository } from 'typeorm';
import Usuario from '../models/Usuario';

import UsuariosRepository from '../repositories/UsuariosRepository';

interface Request {
  nome: string;
  email: string;
  password: string;
}

class CreateUsuarioService {

  public async execute({ nome, email, password }: Request): Promise<Usuario> {
    const usuariosRepository = getCustomRepository(UsuariosRepository);

    const usuario = usuariosRepository.create({
      nome,
      email,
      password,
    });
    await usuariosRepository.save(usuario);

    return usuario;
  }
}

export default CreateUsuarioService;