import { getCustomRepository } from 'typeorm';
import Conta from '../models/Conta';

import ContasRepository from '../repositories/ContasRepository';

interface Request {
  idPai: string;
  nome: string;
  receita: boolean;
  custoFixo: boolean;
}

class CreateContaService {

  public async execute({ idPai, nome, receita, custoFixo }: Request): Promise<Conta> {
    const contasRepository = getCustomRepository(ContasRepository);

    const findContaPai = await contasRepository.findByIdPai(idPai);
        
    var indice = "0";
    if(findContaPai){
      indice = await contasRepository.calculateIndex(findContaPai);
    }

    //const conta = contasRepository.create({      
    //  idPai,
    //  indice,
    //  nome,
    //  receita,
    //  custoFixo,                  
    //});
    const conta = contasRepository.create();
    conta.idPai = idPai;
    conta.indice = indice;
    conta.nome = nome;
    conta.receita = receita;
    conta.custoFixo = custoFixo;    
    await contasRepository.save(conta);

    return conta;
  }
}

export default CreateContaService;